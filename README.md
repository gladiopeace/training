# training

Get your hands dirty with the blockchain!

## Getting started

We follow git-flow at work. Before you start, create a feature branch in the format `feature/issue_number`

## Add your files



```
git clone  https://gitlab.com/PBSA/dapps/training.git
git remote add origin https://gitlab.com/PBSA/dapps/training.git
git branch -M main
git push -uf origin main
```



## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:94947adcd8838d5d6b307e099b06f0ad?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:94947adcd8838d5d6b307e099b06f0ad?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:94947adcd8838d5d6b307e099b06f0ad?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:94947adcd8838d5d6b307e099b06f0ad?https://docs.gitlab.com/ee/user/clusters/agent/)

***

## Visuals
Draw.IO or similar can be used to create mockups
## Installation

If there are build or installation seps, please provde them here.

